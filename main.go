package main
//lol
import (
	"context"
	"fmt"
	"log"
	"strings"

	"github.com/fatih/color"
	"github.com/google/go-github/github"
	"golang.org/x/oauth2"
)

func createClient(token string) *github.Client {
	ctx := context.Background()
	ts := oauth2.StaticTokenSource(
		&oauth2.Token{AccessToken: token},
	)
	tc := oauth2.NewClient(ctx, ts)

	return github.NewClient(tc)
}

func scrapeGitHubRepositories(client *github.Client) error {
	query := "filename:config irc_pass"
	opts := &github.SearchOptions{
		ListOptions: github.ListOptions{PerPage: 100},
	}

	colorFound := color.New(color.FgGreen).Add(color.Bold)
	colorContents := color.New(color.FgCyan).Add(color.Bold)

	for {
		result, response, err := client.Search.Code(context.Background(), query, opts)
		if err != nil {
			return err
		}

		for _, codeResult := range result.CodeResults {
			// Extract relevant information
			repoName := *codeResult.Repository.Name
			filePath := *codeResult.Path
			contentURL := codeResult.HTMLURL

			colorFound.Printf("[+] FOUND: ")
			fmt.Printf("%s -> %s -> %s -> %s ->", *codeResult.Repository.Owner.Login, repoName, filePath, query)
			colorContents.Printf(" %s\n", contentURL)

			matchedLine, err := fetchMatchingLine(*contentURL)
			if err != nil {
				log.Printf("Error fetching matching line: %s\n", err)
				continue
			}

			colorContents.Println(matchedLine)
		}

		if response.NextPage == 0 {
			break
		}

		opts.Page = response.NextPage
	}

	return nil
}

func fetchMatchingLine(url string) (string, error) {
	ctx := context.Background()
	client := github.NewClient(nil)

	apiURL := strings.Replace(url, "github.com", "api.github.com/repos", 1)
	apiURL = strings.Replace(apiURL, "blob/", "", 1)

	fileContent, _, _, err := client.Repositories.GetContents(ctx, "", "", apiURL, &github.RepositoryContentGetOptions{})
	if err != nil {
		return "", err
	}

	contentBytes, err := fileContent.GetContent()
	if err != nil {
		return "", err
	}

	// Find the matching line
	lines := strings.Split(string(contentBytes), "\n")
	for _, line := range lines {
		if strings.Contains(line, "irc_pass") {
			return line, nil
		}
	}

	return "", fmt.Errorf("matching line not found")
}

func main() {
	token := "token here" // Replace with your GitHub token
	client := createClient(token)

	err := scrapeGitHubRepositories(client)
	if err != nil {
		log.Fatal(err)
	}
}
